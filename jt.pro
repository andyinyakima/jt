#-------------------------------------------------
#
# Project created by QtCreator 2016-09-04T14:53:53
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = jt
TEMPLATE = app


SOURCES += main.cpp\
        jt.cpp

HEADERS  += jt.h

FORMS    += jt.ui
