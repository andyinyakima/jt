#include "jt.h"
#include "ui_jt.h"


// placement of the QObject and QProcess is critical
// it has to be in the include section...continued below
QObject *parent;

QProcess *asrin = new QProcess (parent);

// .....and above this section

jt::jt(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::jt)
{

    homepath=QDir::homePath();

 ui->setupUi(this);
 ui->jt_lineEdit->clear();
 ui->jt_lineEdit->insert("This is Beta version 0.43 of julius talker - also known as jt!");

 QFile filein(homepath+"/jt/data/alarm.txt");
 filein.open(QIODevice::ReadOnly | QIODevice::Text);

 QTextStream streamin(&filein);
 while (!streamin.atEnd()) {
           alarm = streamin.readLine();
 }

 ui->alarm_lineEdit->insert(alarm);
 setup_announce();

 /*
  *
  * version .14 has the alarm set and went to am/pm clock versus military time
  * version .15 converted "NEED TIME" to am/pm time also.
  * version .16 "GET" and "NEED" parse the same way for phonetic distinction
  * version .17 change "voice z" to "voice him" because "set pm" was interpreted as "set z am"
  * version .18 added "what" as query versus command
  * version .19 added "urgent" flag
  * version .20 added espeak talker
  * version .21 ability to jump between TTS espeak and flite
  * version .22 remove espeak after saving as jt_v21
  * version .23 added 16 flite voices
  * version .24 change "voice" to "talk" so now I can say "Talk to me (selected voice)" and the selected voice will response
  * version .25 change alarm phrase
  * version .26 using flite 2.0 some raspy sound after a few minutes
  * version .27 added alarm snd played with mpv and Load number reply
  * version .28 some snooze work on alarm
  * version .29 added number command - took out "SET A." replaced with "SET MORNING" sets the alarm for am
  * version .30 added alarm clearing
  * version .31 add in "READ"
  * version .32 volume adjust mute to 100% large fonts for alarm clock and volume
  * version .33 added "padsp" to flite QProcess to allow for oss it helped on laptops and embedded devices 	
  * version .34 rework snooze delay code.
  * version .35 added access to jt-tune
  * version .36 added computer reset  // "RESET COMPUTER"
  * version .37 modified NUMBER/LOAD routine to allow for long numbers; fifteen was not heard properly so now one can
  * say one five and the call back will be fifteen
  * version .38 picks up alarm time (6:15 am) from sub dir /data/alarm.txt and has human voice
  * version .39 added some politeness and change EVE to EVELYN
  * version .40 added a roll call of voices
  * version .41 had roll call default to Alan's voice
  * version .42 after roll call comes back to original voice
  * version .43 added volume audible feed back plus new setting NOMINAL = 75%
  */

 asrline=ui->jt_lineEdit->text();

 vnames<<"ALVIN"<<"ALEX"<<"ACADIE"<<"ALAN"<<"BRIAN"<<"CHRISTINA"<<"EVELYN"<<"FRED"<<"GRANT"<<"LAURIE"<<"RON"<<"RICHARD"<<"STAFF"<<"ANNA"<<"KI"<<"JOHN"<<"MYCROFT";

 //vinitials<<"aew"<<"ahw"<<"aup"<<"awb"<<"bdl"<<"clb"<<"eey"<<"fem"<<"gka"<<"ljm"<<"rxr"<<"rms"<<"slt"<<"axb"<<"ksp"<<"jmk"<<"mycroft_voice_4.0";
 vinitials<<"cmu_us_aew"<<"cmu_us_ahw"<<"cmu_us_aup"<<"cmu_us_awb"<<"cmu_us_bdl"<<"cmu_us_clb"<<"cmu_us_eey"<<"cmu_us_fem"<<"cmu_us_gka"<<"cmu_us_ljm"<<"cmu_us_rxr"<<"cmu_us_rms"<<"cmu_us_slt"<<"cmu_us_axb"<<"cmu_us_ksp"<<"cmu_us_jmk"<<"mycroft_voice_4.0";
 voice="awb";
 talker_flag=0;

 greet();

 sre();


}

jt::~jt()
{
    delete ui;
    asrin->kill(); // this is needed to kill instance of julius
                   // other wise there will be too many copies.
}

void jt::sre()
{
   // let's set up julius
    QString confpath = QDir::homePath();
    confpath.append("/jt_base.jconf");

    QString prog = "julius";
    QString conf = "-C";

    QStringList argu;


    argu<<conf<<confpath;


  //  QProcess *asrin = new QProcess(this);
    asrin->setEnvironment(QProcess::systemEnvironment());
    asrin->setProcessChannelMode(QProcess::MergedChannels);

    asrin->start(prog,argu);


    connect(asrin,SIGNAL(readyReadStandardOutput()),this,SLOT(readsre()));
    connect(asrin,SIGNAL(finished(int)),this,SLOT(stopsre()));

}

void jt::readsre()
{
    int foundstart;
    int foundend;

    QString line;
    QByteArray pile;
    QStringList lines;

    QProcess *asrin = dynamic_cast<QProcess *>(sender());


    if(asrin)
    {

       pile=asrin->readAllStandardOutput();
       lines=QString(pile).split("\n");
       foreach (line, lines) {
           if(line.contains("sentence1"))
           {
               foundstart = line.indexOf("<s>")+3;
               foundend = line.indexOf("</s>");
               line.resize(foundend);
               line.remove(0,foundstart);

               asrline=line;

           ui->jt_lineEdit->insert(asrline);
           }

       }
        ui->jt_lineEdit->insert(asrin->readAllStandardOutput());

    }
    parse();
}

void jt::stopsre()
{
    asrin->waitForFinished();
    if(asrin->state()!= QProcess::NotRunning)
        asrin->kill();
}

void jt::parse()
{
    if(asrline.contains("THANK")==true)
        parse_polite();

    if(asrline.contains("ROLL")==true)
        parse_query();

    if(asrline.contains("BOS")==true)
    {
        parse_bos();
    }
    if(asrline.contains("NEED")==true)
    {
        asrline.remove(0,5);
        parse_need();
    }

    if(asrline.contains("GET")==true)
    {
        asrline.remove(0,4);
        parse_need();
    }

    if(asrline.contains("WHAT")==true)
    {
        asrline.remove(0,5);
        parse_query();
    }
    if(asrline.contains("HELLO")==true)
    {
       // asrline.remove(0,5);
        parse_query();
    }

    else if(asrline.contains("RESET")==true)
    {

        reset_computer();
    }

    else if(asrline.contains("LOAD")==true)
    {
        asrline.remove(0,5);
        parse_load();
    }

    else if(asrline.contains("NUMBER")==true)
    {
        asrline.remove(0,7);
        parse_load();
    }

    else if(asrline.contains("SET")==true)
    {
        asrline.remove(0,4);
        parse_set();
    }
    else if(asrline.contains("ANNOUNCE")==true)
    {
        asrline.remove(0,9); //remove word "ANNOUNCE"
        parse_announce();
    }

    else if(asrline.contains("SPEAK")==true)
    {
        //asrline.remove(0,6); // remove word "SPEAK"
        parse_voice();
    }

    else if(asrline.contains("TALK")==true)
    {
       // asrline.remove(0,6); // remove word "VOICE"
        parse_voice();
    }

    else if(asrline.contains("CLEAR")==true)
    {
       // asrline.remove(0,6); // remove word "VOICE"
        parse_clear();
    }

    else if(asrline.contains("READ")==true)
    {
        parse_read();
    }

    else if(asrline.contains("VOLUME")==true)
    {
        set_volume();
    }


    else
    {
        //ui->jt_lineEdit->clear();
        ui->jt_lineEdit->insert(asrline);


        greet();

    }

}

void jt::parse_bos()
{
    QString line = asrline;
    asrline.clear();

    if(line.contains("WA")==true)
        setup_boswasi();
}

void jt::parse_need()
{
    QString line = asrline;

    if(line.contains("FORMULA")==true)
        setup_formula();

    if(line.contains("TUNER")==true)
        setup_tuner();

    if(line.contains("MATH")==true)
        setup_math();

    else if(line.contains("TIME")==true)
    {
        urgent++;
        date_time();
    }

    else if(line.contains("DATE")==true)
    {
        urgent++;
        date_time();
    }

    else
    {
        //ui->jt_lineEdit->clear();
        ui->jt_lineEdit->insert(asrline);


    }

   greet();
}

void jt::parse_query()
{
    QString line = asrline;

    if(line.contains("TIME")==true)
        date_time();
    else if(line.contains("DATE")==true)
        date_time();
    else if(line.contains("HELLO")==true)
        talker();
    else if(line.contains("CALL")==true)
        roll_call();

}


void jt::parse_voice()
{
    QString line = asrline;
    asrline.clear();
    QString voicename = homepath+"/";
    QString finish = ".flitevox";



    if(line.contains("ALVIN")==true) // vnum=1
    {
        voice=voicename+"cmu_us_aew"+finish;
        line.clear();
        line = "You have picked a voice that has been named Alvin! Until you pick another voice this is the voice you will hear!";
    }

    else if(line.contains("ALEX")==true) // vnum=2
    {
        voice=voicename+"cmu_us_ahw"+finish;
        line.clear();
        line = "You have picked a voice that has been named Alex! Until you pick another voice this is the voice you will hear!";
    }

    else if(line.contains("ACADIE")==true) // vnum=3
    {
        voice=voicename+"cmu_us_aup"+finish;
        line.clear();
        line = "You have picked a voice that has been named Acadie! Until you pick another voice this is the voice you will hear!";
    }

    else if(line.contains("ALAN")==true) // vnum=4
    {
        voice=voicename+"cmu_us_awb"+finish;
        line.clear();
        line = "You have picked a voice that has been named Alan! Until you pick another voice this is the voice you will hear!";
    }


    else if(line.contains("BRIAN")==true)  // vnum=5
    {
        voice=voicename+"cmu_us_bdl"+finish;
        line.clear();
        line = "You have picked a voice that has been named Brian! Until you pick another voice this is the voice you will hear!";
    }

    else if(line.contains("CHRISTINA")==true) // vnum=6
    {
        voice=voicename+"cmu_us_clb"+finish;
        line.clear();
        line = "You have picked a voice that has been named Christina! Until you pick another voice this is the voice you will hear!";
    }

    else if(line.contains("EVELYN")==true) // vnum=7
    {
        voice=voicename+"cmu_us_eey"+finish;
        line.clear();
        line = "You have picked a voice that has been named Evelyn! Until you pick another voice this is the voice you will hear!";
    }

    else if(line.contains("FRED")==true) // vnum=8
    {
        voice=voicename+"cmu_us_fem"+finish;
        line.clear();
        line = "You have picked a voice that has been named Fred! Until you pick another voice this is the voice you will hear!";
    }

    else if(line.contains("GRANT")==true) // vnum=9
    {
        voice=voicename+"cmu_us_gka"+finish;
        line.clear();
        line = "You have picked a voice that has been named Grant! Until you pick another voice this is the voice you will hear!";
    }

    else if(line.contains("LAURIE")==true)  // vnum=10
    {
        voice=voicename+"cmu_us_ljm"+finish;
        line.clear();
        line = "You have picked a voice that has been named Laurie! Until you pick another voice this is the voice you will hear!";
    }

    else if(line.contains("RON")==true)  // vnum=11
    {
        voice=voicename+"cmu_us_rxr"+finish;
        line.clear();
        line = "You have picked a voice that has been named Ron! Until you pick another voice this is the voice you will hear!";
    }

    else if(line.contains("RICHARD")==true) // vnum=12
    {
        voice=voicename+"cmu_us_rms"+finish;
        line.clear();
        line = "You have picked a voice that has been named Richard! Until you pick another voice this is the voice you will hear!";
    }

    else if(line.contains("STAFF")==true) // vnum=13
    {
        voice=voicename+"cmu_us_slt"+finish;
        line.clear();
        line = "You have picked a voice that has been named Stefanie! Until you pick another voice this is the voice you will hear!";
    }
    else if(line.contains("ANNA")==true) // vnum=14
    {
        voice=voicename+"cmu_us_axb"+finish;
        line.clear();
        line = "You have picked a voice that has been named Anna! Until you pick another voice this is the voice you will hear!";
    }

    else if(line.contains("KI")==true) // vnum=15
    {
        voice=voicename+"cmu_us_ksp"+finish;
        line.clear();
        line = "You have picked a voice that has been named Kishore! Until you pick another voice this is the voice you will hear!";
    }

    else if(line.contains("JOHN")==true) // vnum=16
    {
        voice=voicename+"cmu_us_jmk"+finish;
        line.clear();
        line = "You have picked a voice that has been named John! Until you pick another voice this is the voice you will hear!";
    }

    else if(line.contains("MYCROFT")==true) // vnum=17
    {
        voice=voicename+"mycroft_voice_4.0"+finish;
        line.clear();
        line = "You have picked a voice that has been named Mycroft! Until you pick another voice this is the voice you will hear!";
    }
    asrline=line;

    greet();

}

void jt::parse_announce()
{
    QString line =asrline;
    asrline.clear();



    if(line.contains("MINUTE")==true)
    {
        announce_len =1;
        asrline="The time will be announced every minute.";
        setup_announce();

    }
    if(line.contains("QUARTER")==true)
    {
        announce_len =15;
        asrline="The time will be announced every hour, quarter pass the hour, on the half hour, and 15 minutes before the hour!";
        setup_announce();

    }

    if(line.contains("HALF")==true)
    {
        announce_len =30;
        asrline="The time will be announced every hour, and half pass the hour!";
        setup_announce();

    }

    if(line.contains("TOP")==true)
    {
        announce_len =60;
        asrline="The time will be announced every hour!";
        setup_announce();

    }

    if(line.contains("SPEAKER")==true)
    {

        talker();
    }

}

void jt::parse_load()
{
    num_digit=0;
    num_ty=0;
    QString line = asrline;
    bool ok;


        if(line.contains("TWENTY")==true)
        {
           line.remove(0,7);
           num_ty = 20;

        }
        if(line.contains("THIRTY")==true)
        {
           line.remove(0,7);
           num_ty = 30;
        }
        if(line.contains("FOURTY")==true)
        {
            line.remove(0,7);
            num_ty = 40;
        }
        if(line.contains("FIFTY")==true)
        {
           line.remove(0,6);
           num_ty = 50;
        }
        if(line.contains("SIXTY")==true)
        {
           line.remove(0,6);
           num_ty = 60;

        }
        if(line.contains("SEVENTY")==true)
        {
           line.remove(0,8);
           num_ty = 70;
        }
        if(line.contains("EIGHTY")==true)
        {
            line.remove(0,7);
            num_ty = 80;
        }
        if(line.contains("NINETY")==true)
        {
           line.remove(0,7);
           num_ty = 90;
        }

      //     line.replace("POINT",".");
           line.replace("THIRTEEN",QString::number(13));
           line.replace("FOURTEEN",QString::number(14));
           line.replace("FIFTEEN",QString::number(15));
           line.replace("SIXTEEN",QString::number(16));
           line.replace("SEVENTEEN",QString::number(17));
           line.replace("EIGHTEEN",QString::number(18));
           line.replace("NINETEEN",QString::number(19));
           line.replace("ZERO",QString::number(0));
           line.replace("OTT",QString::number(0));
      //     line.replace("OH",QString::number(0));
           line.replace("ONE",QString::number(1));
           line.replace("TWO",QString::number(2));
           line.replace("THREE",QString::number(3));
           line.replace("FOUR",QString::number(4));
           line.replace("FIVE",QString::number(5));
           line.replace("SIX",QString::number(6));
           line.replace("SEVEN",QString::number(7));
           line.replace("EIGHT",QString::number(8));
           line.replace("NINE",QString::number(9));
           line.replace("TEN",QString::number(10));
           line.replace("ELEVEN",QString::number(11));
           line.replace("TWELVE",QString::number(12));



           line.replace(" ","");
           num_digit = line.toInt(&ok);
           num_load = num_ty + num_digit;
           line=QString::number(num_load);


           load_time.clear();
           load_time=line;
           asrline="The number "+line+" has been loaded!";

           greet();
}


void jt::parse_set()
{

    QString line = asrline;


    asrline.clear();

    if(line.contains("HOUR")==true)
    {
        line_hour.clear();
        hour=num_load;
        asrline="The alarm hour has been set to ";
        line_hour=QString::number(hour);
        asrline.append(line_hour+".");


    }
    else if(line.contains("MINUTE")==true)
    {
       line_min.clear();
        minute=num_load;
        asrline="The alarm minute has been set to ";
        line_min=QString::number(minute);
        if(line_min.length()==1)
            line_min.prepend("0");
        asrline.append(line_min+".");
        ui->tt1plainTextEdit->clear();
        ui->tt1plainTextEdit->insertPlainText(asrline);
    }

    else if(line.contains("MORNING")==true)
    {
        line.clear();
        ampm="am";
        line="The alarm clock has been set to ";
        ui->alarm_lineEdit->clear();
        ui->alarm_lineEdit->insert(line_hour+":"+line_min+" "+ampm);
        line.append(ui->alarm_lineEdit->text()+"; and will be announced when it occurs!");
        asrline=line;
        setup_announce();

    }
    else if(line.contains("PM")==true)
    {
        line.clear();
        ampm="pm";
        line="The alarm clock has been set to ";
        ui->alarm_lineEdit->clear();
        ui->alarm_lineEdit->insert(line_hour+":"+line_min+" "+ampm);
        line.append(ui->alarm_lineEdit->text()+"; and will be announced when it occurs!");
        asrline=line;
        setup_announce();
    }

    else if(line.contains("DELAY")==true)
    {
        line.clear();
        snuz = num_load;
        line=QString::number(snuz)+" minutes.";
        line.prepend("The alarm snooze delay is ");
        asrline=line;

    }
/*
    else if(line.contains("VOLUME")==true)
    {
        volume = num_load;
        set_volume();
    }
*/
    greet();

}

void jt::parse_read()
{
    QString line =asrline;

    asrline.clear();

    if(line.contains("DICTIONARY")==true)
    {
        talker_flag++;
        filepath=homepath+"/jt/KEYWORDS";

    }

    else if(line.contains("STORY")==true)
    {
        talker_flag++;
        filepath=homepath+"/jt/STORY";

    }

    greet();
}

void jt::parse_polite()
{
    QString line = asrline;
    asrline.clear();

    if(line.contains("YOU")==true)
    {
        line = "You're Welcome!";
    }

    asrline = line;
    greet();
}


void jt::greet()
{
  if(talker_flag==0)
    flite1talk();
  else if(talker_flag!=0)
      flitefile();
}

void jt::roll_call()
{
    int cnt;
    int indx;

    QString voicename = homepath+"/";
    QString finish = ".flitevox";
    QString middle;
    QString vtemp=voice;


    for(cnt=1;cnt<18;cnt++)
    {
        voice.clear();
        indx=cnt-1;
        middle=vinitials.at(indx);
        voice=voicename+middle+finish;
        talker();
        sleep(3);

    }
    voice=vtemp;
    asrline=" I am the original speaker before roll call!";
    greet();
    talker();
}

void jt::talker()
{
    QString line = "Hi... my name is ";
    asrline.clear();
    if(voice.contains("aew")==true)
        line.append("Alvin");
    else if(voice.contains("ahw")==true)
        line.append("Alex");
    else if(voice.contains("aup")==true)
        line.append("Acadie");
    else if(voice.contains("awb")==true)
        line.append("Alan");
    else if(voice.contains("axb")==true)
        line.append("Anna");
    else if(voice.contains("bdl")==true)
        line.append("Brian");
    else if(voice.contains("clb")==true)
        line.append("Christina");
    else if(voice.contains("eey")==true)
        line.append("Evelyn");
    else if(voice.contains("fem")==true)
        line.append("Fred");
    else if(voice.contains("gka")==true)
        line.append("Grant");
    else if(voice.contains("jmk")==true)
        line.append("John");
    else if(voice.contains("ksp")==true)
        line.append("Kishore");
    else if(voice.contains("ljm")==true)
        line.append("Laurie");
    else if(voice.contains("rms")==true)
        line.append("Richard");
    else if(voice.contains("rxr")==true)
        line.append("Ron");
    else if(voice.contains("slt")==true)
        line.append("Stefanie");
    else if(voice.contains("mycroft")==true)
        line.append("Mycroft");

    line.append(".. and I am a host speaker!");
    asrline=line;
    greet();
}




/*
flite: a small simple speech synthesizer
  Carnegie Mellon University, Copyright (c) 1999-2011, all rights reserved
  version: flite-2.0.0-release Dec 2014 (http://cmuflite.org)
usage: flite TEXT/FILE [WAVEFILE]
  Converts text in TEXTFILE to a waveform in WAVEFILE
  If text contains a space the it is treated as a literal
  textstring and spoken, and not as a file name
  if WAVEFILE is unspecified or "play" the result is
  played on the current systems audio device.  If WAVEFILE
  is "none" the waveform is discarded (good for benchmarking)
  Other options must appear before these options
  --version   Output flite version number
  --help      Output usage string
  -o WAVEFILE Explicitly set output filename
  -f TEXTFILE Explicitly set input filename
  -t TEXT     Explicitly set input textstring
  -p PHONES   Explicitly set input textstring and synthesize as phones
  --set F=V   Set feature (guesses type)
  -s F=V      Set feature (guesses type)
  --seti F=V  Set int feature
  --setf F=V  Set float feature
  --sets F=V  Set string feature
  -ssml       Read input text/file in ssml mode
  -b          Benchmark mode
  -l          Loop endlessly
  -voice NAME Use voice NAME (NAME can be filename or url too)
  -voicedir NAME Directory contain voice data
  -lv         List voices available
  -add_lex FILENAME add lex addenda from FILENAME
  -pw         Print words
  -ps         Print segments
  -psdur      Print segments and their durations (end-time)
  -pr RelName Print relation RelName
  -voicedump FILENAME Dump selected (cg) voice to FILENAME
  -v          Verbose mode


*/
void jt::flite1talk()
{

    QString prg = "padsp"; //padsp was added so oss systems will play flite 2.0
    QString prog = "flite";
    QString readfile ="-t";
    QString voicecall = "-voice";
    QString voicename;
    QString setstuff = "--seti";
    QStringList argu;


    argu<<prog<<voicecall<<voice<<setstuff<<readfile<<asrline;
   // argu<<asrline;

    QProcess *flt1 = new QProcess(this);
        flt1->start(prg,argu);
        asrline.clear();

    sleep(3);
}

void jt::flitefile()
{
    QString prg = "padsp"; //padsp was added so oss systems will play flite 2.0
    QString prog = "flite";
    QString readfile ="-f";
    QString voicecall = "-voice";

    QString setstuff = "--seti";
    QStringList argu;


    argu<<prog<<voicecall<<voice<<setstuff<<readfile<<filepath;
   // argu<<asrline;

    QProcess *flt1 = new QProcess(this);
        flt1->start(prg,argu);
        asrline.clear();

    talker_flag=0;
    sleep(3);

}

/*
 * espeak
 * Voice X = voice = en-us+f5
 * Voice Y = voice = en-us
 * Voice HIM = voice = en-sc


void jt::espeaker()
{
    QString prog = "espeak";
    QString voicecall = "-v";
    QString voicename ="en-us+f5";
    QString speed = "-s130";
    QString pitch = "-p50";

    if(voice=="x")
        voicename="en-us+f5";
    if(voice=="y")
        voicename="en-us";
    if(voice=="him")
        voicename="en-sc";


    QStringList argu;

    argu<<voicecall<<voicename<<speed<<pitch<<asrline;

    QProcess *speak = new QProcess(this);
    speak->start(prog,argu);
    asrline.clear();

    sleep(3);

}
*/

void jt::set_volume()
{
    QString line =asrline;
    asrline.clear();

    QString prog = "amixer";
    QString dee = "-D";
    QString pulse = "pulse";
    QString sset = "sset";
    QString master = "Master";
    QString percent = "%";

    QStringList argu;

    if(line.contains("MUTE")==true)
    {
        volume=0;
    }

    else if(line.contains("NOMINAL")==true)
    {
        volume=75;
    }

    else if(line.contains("TOP")==true)
    {
        volume=100;
    }

    else if(line.contains("INCREASE")==true)
    {
        volume=volume+10;
    }

    else if(line.contains("QUARTER")==true)
    {
        volume=25;
    }

    else if(line.contains("HALF")==true)
    {
        volume=50;
    }
    percent.prepend(QString::number(volume));

    argu<<dee<<pulse<<sset<<master<<percent;

    ui->vol_lineEdit->clear();
    ui->vol_lineEdit->insert(percent);

    QProcess *vol_call = new QProcess(this);

    vol_call->start(prog,argu);

    asrline = "Volume ";
    asrline.append(ui->vol_lineEdit->text());

    greet();

}


void jt::setup_announce()
{
    greet();

        QTimer *timer = new QTimer(this);

        connect(timer, SIGNAL(timeout()), this, SLOT(announce_time()));
        timer->start(1000);






}


void jt::announce_time()
{
    QString cs = QTime::currentTime().toString("ss");
    QString ct = QTime::currentTime().toString("mm");
    QString ca = QTime::currentTime().toString("h:mm ap");

    if(cs=="00")
    {
        if(announce_len==1)
        {
            ui->jt_lineEdit->clear();
            asrline.clear();
            ui->jt_lineEdit->insert(" The time is, ");
            ct = QTime::currentTime().toString("h:mm ap");
            ui->jt_lineEdit->insert(ct+". \n");
            asrline = ui->jt_lineEdit->text();
        }

        if(announce_len==15)
        {
            if(ct=="00" || ct=="15" || ct=="30" || ct=="45")
            {
                ui->jt_lineEdit->clear();
                asrline.clear();
                ui->jt_lineEdit->insert(" The time is, ");
                ct = QTime::currentTime().toString("h:mm ap");
                ui->jt_lineEdit->insert(ct+". \n");
                asrline = ui->jt_lineEdit->text();
            }
        }

        if(announce_len==30)
        {
            if(ct=="00" || ct=="30")
            {
                ui->jt_lineEdit->clear();
                asrline.clear();
                ui->jt_lineEdit->insert(" The time is, ");
                ct = QTime::currentTime().toString("h:mm ap");
                ui->jt_lineEdit->insert(ct+". \n");
                asrline = ui->jt_lineEdit->text();
            }
        }

        if(announce_len==60)
        {
            if(ct=="00")
            {
                ui->jt_lineEdit->clear();
                asrline.clear();
                ui->jt_lineEdit->insert(" The time is, ");
                ct = QTime::currentTime().toString("h:mm ap");
                ui->jt_lineEdit->insert(ct+". \n");
                asrline = ui->jt_lineEdit->text();
            }
        }
        if(ca==ui->alarm_lineEdit->text())
        {

            sound_alarm();
            asrline.clear();
            ui->tt1plainTextEdit->clear();
            ui->tt1plainTextEdit->insertPlainText("Clickety Clack.....Clickety Clack..\n");
            ui->tt1plainTextEdit->insertPlainText("There's a train thats a coming' .....On down the track..\n");
            ui->tt1plainTextEdit->insertPlainText(" The Alarm Time is "+ui->alarm_lineEdit->text()+" .");
            asrline = ui->tt1plainTextEdit->toPlainText();
            greet();
            snooze();

        }
    }

    greet();
}

void jt::date_time()
{
    QString line = asrline;

    ui->jt_lineEdit->clear();
    asrline.clear();

    if(line.contains("TIME")==true)
    {

        if(urgent==0)
            ui->jt_lineEdit->insert(" The time is, ");
        QString ct = QTime::currentTime().toString("h:mm ap");
        ui->jt_lineEdit->insert(ct+". \n");

    }
    else if(line.contains("DATE")==true)
    {

        if(urgent==0)
            ui->jt_lineEdit->insert("The date is ");
        QString cd = QDate::currentDate().toString("dddd MMMM dd yyyy");
        ui->jt_lineEdit->insert(cd+". \n");

    }
     urgent=0;
     asrline = ui->jt_lineEdit->text();
     greet();
}


void jt::parse_clear()
{
    QString line = asrline;


    asrline.clear();

    if(line.contains("ALARM")==true)
    {
        minute=0;
        hour=0;
        line_hour.clear();
        line_min.clear();
        ampm.clear();
        ui->alarm_lineEdit->clear();
        asrline="The alarm has been cleared!";

    }
    greet();
}

void jt::sound_alarm()
{
    QString prog = "mpv";
    QString wav = homepath+"/jt/data/train.mp3";
    QStringList argu;

    argu<<wav;

    QProcess *snd_alrm = new QProcess(this);
        snd_alrm->start(prog,argu);

    sleep(3);


}

void jt::snooze()
{

    int secs = snuz*60;


    QString ca = QTime::currentTime().addSecs(secs).toString("h:mm ap");

    sleep(30);
    ui->alarm_lineEdit->clear();
    ui->alarm_lineEdit->insert(ca);




}


void jt::setup_boswasi()
{
    asrline="Calling boswasi";
    greet();
    sleep(3);

    QProcess *boswasi_call = new QProcess(this);
    QString prog = "boswasi";

    boswasi_call->startDetached(prog);
    sleep(3);
    QCoreApplication::exit();
}

void jt::setup_math()
{


    asrline="Calling jt-math!";

    greet();

    sleep(3);

    QProcess *math_call = new QProcess (this);
    QString prog = "jt-math";

    math_call->startDetached(prog);
    sleep(3);
    QCoreApplication::exit();

}

void jt::setup_formula()
{


    asrline="Calling jt_formula!";

    greet();

    sleep(3);

    QProcess *formula_call = new QProcess (this);
    QString prog = "jt-formula";

    formula_call->startDetached(prog);
    sleep(3);
    QCoreApplication::exit();

}

void jt::setup_tuner()
{
    asrline="Calling jt-tune...!";

    greet();

    sleep(3);

    QProcess *tuner_call = new QProcess (this);
    QString prog = "jt-tune";

    tuner_call->startDetached(prog);
    sleep(3);
    QCoreApplication::exit();

}

void jt::reset_computer()
{

    QString prog = "shutdown";
    QString reboot = "-r";
    QString when = "now";

    QStringList argu;

    argu<<reboot<<when;

    QProcess *process = new QProcess(this);

    if(asrline.contains("COMPUTER")==true)
    {

        process->startDetached(prog,argu);
    }





}
