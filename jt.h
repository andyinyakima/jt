#ifndef JT_H
#define JT_H

#include <QApplication>
#include <QMainWindow>
#include <QDesktopWidget>
#include <QFileSystemWatcher>
#include <QDateTime>
#include <QTimer>
#include <QProcess>
#include <QTextStream>
#include <QFile>
#include <QDir>
#include <unistd.h>


namespace Ui {
class jt;
}

class jt : public QMainWindow
{
    Q_OBJECT

public:
    explicit jt(QWidget *parent = 0);
    ~jt();

private slots:

 //   void readasrin();

    void sre();

    void readsre();

    void stopsre();

    void parse();

    void parse_bos();

    void parse_need();

    void parse_query();

    void parse_voice();

    void parse_announce();

    void parse_load();

    void parse_set();

    void parse_read();

    void parse_polite();

    void greet();

    void roll_call();

    void talker();

    void flite1talk();

    void flitefile();

    void set_volume();

    void setup_announce();

    void announce_time();

    void date_time();

    void parse_clear();

    void sound_alarm();

    void snooze();

    void setup_boswasi();

    void setup_math();

    void setup_formula();

    void setup_tuner();
    
    void reset_computer();


private:
    int pos_asrin;
    int size_old;
    int size_new;
    int announce_len=0;
    int timer_flag =0;
    int hour;
    int minute;
    int snuz=5;
    int num_load;
    int num_ty;
    int num_digit;
    int urgent=0; // flag for urgency
    int talker_flag; // 0=flite 1=espeak
    int volume;
    int vnum;  // voice number 1-17



    QString homepath;
    QString filepath;
    QString voice;
    QString asrline;  // This is a global string for carrying flite's voice.
    QString load_time;
    QString ampm;
    QString line_hour;
    QString line_min;
    QString alarm;

    QStringList vnames;
    QStringList vinitials;

  //  QTimer *timer;






    Ui::jt *ui;
};

#endif // JT_H
